/*
 * Copyright © 2010 Google
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <vulkan/vulkan.h>
#include <vulkan/vk_layer.h>

#include <cassert>
#include <cstdio>
#include <cstring>
#include <mutex>
#include <unordered_map>

struct Instance {
   PFN_vkGetInstanceProcAddr getInstanceProcAddr;
   PFN_vkDestroyInstance destroyInstance;
   PFN_vkCreateDevice createDevice;
   PFN_vkDestroyDevice destroyDevice;
};

struct Device {
   PFN_vkGetDeviceProcAddr getDeviceProcAddr;
   PFN_vkDestroyDevice destroyDevice;
   PFN_vkAllocateMemory allocateMemory;
   PFN_vkFreeMemory freeMemory;
   PFN_vkCreateImage createImage;
   PFN_vkDestroyImage destroyImage;
   PFN_vkGetImageMemoryRequirements getImageMemoryRequirements;
   PFN_vkGetImageMemoryRequirements2 getImageMemoryRequirements2;
   PFN_vkBindImageMemory bindImageMemory;
   PFN_vkBindImageMemory2 bindImageMemory2;
};

struct Memory {
   bool shared;
   unsigned type;
};

struct Image {
   bool eligible;
   VkDeviceMemory deviceMemory;
};

std::mutex g_mutex;
std::unordered_map<VkInstance, Instance> g_instance;
std::unordered_map<VkDevice, Device> g_device;
std::unordered_map<VkDeviceMemory, Memory> g_memory;
std::unordered_map<VkImage, Image> g_image;

static Device *get_device(VkDevice device)
{
   std::unique_lock<std::mutex> l(g_mutex);
   auto it = g_device.find(device);
   return it != g_device.end() ? &it->second : nullptr;
}


static VkResult
layer_AllocateMemory(VkDevice device,
                     const VkMemoryAllocateInfo* pAllocateInfo,
                     const VkAllocationCallbacks* pAllocator,
                     VkDeviceMemory* pMem)
{
   auto dev = get_device(device);
   assert(dev);

   VkResult result = dev->allocateMemory(device, pAllocateInfo, pAllocator, pMem);
   if (result != VK_SUCCESS)
      return result;

   Memory memory = {};

   memory.shared = false;
   memory.type = pAllocateInfo->memoryTypeIndex;
   {
      std::unique_lock<std::mutex> l(g_mutex);
      g_memory[*pMem] = memory;
   }
   return VK_SUCCESS;
}

static void
layer_FreeMemory(VkDevice device,
                 VkDeviceMemory mem,
                 const VkAllocationCallbacks* pAllocator)
{
   auto dev = get_device(device);
   assert(dev);

   dev->freeMemory(device, mem, pAllocator);
   {
      std::unique_lock<std::mutex> l(g_mutex);
      auto it = g_memory.find(mem);
      g_memory.erase(it);
   }
}

static VkResult
layer_CreateImage(VkDevice device,
                  const VkImageCreateInfo *pCreateInfo,
                  const VkAllocationCallbacks *pAllocator,
                  VkImage *pImage)
{
   auto dev = get_device(device);
   assert(dev);

   VkResult result = dev->createImage(device, pCreateInfo, pAllocator, pImage);
   if (result != VK_SUCCESS)
      return result;

   Image image = {};
   image.eligible = true;
   if (pCreateInfo->tiling != VK_IMAGE_TILING_OPTIMAL ||
       (pCreateInfo->flags & VK_IMAGE_CREATE_ALIAS_BIT) ||
       (pCreateInfo->flags & VK_IMAGE_CREATE_SPARSE_BINDING_BIT))
      image.eligible = false;

   {
      std::unique_lock<std::mutex> l(g_mutex);
      g_image[*pImage] = image;
   }

   return VK_SUCCESS;
}

static void
layer_DestroyImage(VkDevice device,
                   VkImage image,
                   const VkAllocationCallbacks *pAllocator)
{
   auto dev = get_device(device);
   assert(dev);

   dev->destroyImage(device, image, pAllocator);
   {
      std::unique_lock<std::mutex> l(g_mutex);
      auto it = g_image.find(image);

      if (it->second.deviceMemory) {
         dev->freeMemory(device, it->second.deviceMemory, nullptr);
      }
      g_image.erase(it);
   }
}

static void
layer_GetImageMemoryRequirements(VkDevice device,
                                 VkImage image,
                                 VkMemoryRequirements* pMemoryRequirements)
{
   auto dev = get_device(device);
   assert(dev);

   Image *im = nullptr;
   {
      std::unique_lock<std::mutex> l(g_mutex);
      auto it = g_image.find(image);
      assert(it != g_image.end());
      im = &it->second;
   }

   dev->getImageMemoryRequirements(device, image, pMemoryRequirements);
   if (im->eligible) {
      pMemoryRequirements->size = 1;
      pMemoryRequirements->alignment = 1;
   }
}

static void
layer_GetImageMemoryRequirements2(VkDevice device,
                                  const VkImageMemoryRequirementsInfo2 *pInfo,
                                  VkMemoryRequirements2 *pMemoryRequirements)
{
   auto dev = get_device(device);
   assert(dev);

   Image *im = nullptr;
   {
      std::unique_lock<std::mutex> l(g_mutex);
      auto it = g_image.find(pInfo->image);
      assert(it != g_image.end());
      im = &it->second;
   }

   dev->getImageMemoryRequirements2(device, pInfo, pMemoryRequirements);
   if (im->eligible) {
      pMemoryRequirements->memoryRequirements.size = 1;
      pMemoryRequirements->memoryRequirements.alignment = 1;
   }
}


static VkResult
layer_BindImageMemory(VkDevice device,
                      VkImage image,
                      VkDeviceMemory memory,
                      VkDeviceSize  memoryOffset)
{
   auto dev = get_device(device);
   assert(dev);

   Image *im = nullptr;
   Memory *mem = nullptr;
   {
      std::unique_lock<std::mutex> l(g_mutex);
      auto it = g_image.find(image);
      assert(it != g_image.end());
      im = &it->second;
      auto it2 = g_memory.find(memory);
      assert(it2 != g_memory.end());
      mem = &it2->second;
   }

   if (!im->eligible)
      return dev->bindImageMemory(device, image, memory, memoryOffset);

   VkMemoryRequirements reqs;

   dev->getImageMemoryRequirements(device, image, &reqs);

   VkMemoryDedicatedAllocateInfo ded_info = {};
   ded_info.sType = VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO;
   ded_info.image = image;

   VkMemoryAllocateInfo alloc_info = {};
   alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
   alloc_info.pNext = &ded_info;
   alloc_info.allocationSize = reqs.size;
   alloc_info.memoryTypeIndex = mem->type;

   VkResult result = dev->allocateMemory(device, &alloc_info, nullptr, &im->deviceMemory);
   if (result != VK_SUCCESS)
      return result;

   return dev->bindImageMemory(device, image, im->deviceMemory, 0);
}

static VkResult
layer_BindImageMemory2(VkDevice device,
                      uint32_t bindInfoCount,
                      const VkBindImageMemoryInfo *pBindInfos)
{
   auto dev = get_device(device);
   assert(dev);

   for (uint32_t i = 0; i < bindInfoCount; ++i) {
      Image *im = nullptr;
      Memory *mem = nullptr;
      VkResult result;
      {
         std::unique_lock<std::mutex> l(g_mutex);
         auto it = g_image.find(pBindInfos[i].image);
         assert(it != g_image.end());
         im = &it->second;
         auto it2 = g_memory.find(pBindInfos[i].memory);
         assert(it2 != g_memory.end());
         mem = &it2->second;
      }
      if (!im->eligible) {
         result = dev->bindImageMemory2(device, 1, pBindInfos + i);
         if (result != VK_SUCCESS)
            return result;
         continue;
      }

         VkMemoryRequirements reqs;

      dev->getImageMemoryRequirements(device, pBindInfos[i].image, &reqs);

      VkMemoryDedicatedAllocateInfo ded_info = {};
      ded_info.sType = VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO;
      ded_info.image = pBindInfos[i].image;

      VkMemoryAllocateInfo alloc_info = {};
      alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
      alloc_info.pNext = &ded_info;
      alloc_info.allocationSize = reqs.size;
      alloc_info.memoryTypeIndex = mem->type;

      result = dev->allocateMemory(device, &alloc_info, nullptr, &im->deviceMemory);
      if (result != VK_SUCCESS)
         return result;

      result = dev->bindImageMemory(device, pBindInfos[i].image, im->deviceMemory, 0);
      if (result != VK_SUCCESS)
         return result;
   }

   return VK_SUCCESS;
}

static VkResult
layer_CreateDevice(VkPhysicalDevice physicalDevice,
                   const VkDeviceCreateInfo* pCreateInfo,
                   const VkAllocationCallbacks* pAllocator,
                   VkDevice* pDevice)
{
   VkLayerDeviceCreateInfo *layer_create_info = (VkLayerDeviceCreateInfo *)pCreateInfo->pNext;

   while(layer_create_info &&
         (layer_create_info->sType != VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO ||
          layer_create_info->function != VK_LAYER_LINK_INFO)) {
      layer_create_info = (VkLayerDeviceCreateInfo *)layer_create_info->pNext;
   }

   if(layer_create_info == NULL)
      return VK_ERROR_INITIALIZATION_FAILED;

   PFN_vkGetInstanceProcAddr getInstanceProcAddr = layer_create_info->u.pLayerInfo->pfnNextGetInstanceProcAddr;
   PFN_vkGetDeviceProcAddr getDeviceProcAddr = layer_create_info->u.pLayerInfo->pfnNextGetDeviceProcAddr;
   layer_create_info->u.pLayerInfo = layer_create_info->u.pLayerInfo->pNext;

   PFN_vkCreateDevice createDevice = (PFN_vkCreateDevice)getInstanceProcAddr(VK_NULL_HANDLE, "vkCreateDevice");

   VkResult result = createDevice(physicalDevice, pCreateInfo, pAllocator, pDevice);
   if (result != VK_SUCCESS)
      return result;

   Device device = {};
   device.getDeviceProcAddr = getDeviceProcAddr;
   device.destroyDevice = (PFN_vkDestroyDevice)getDeviceProcAddr(*pDevice, "vkDestroyDevice");
   device.allocateMemory = (PFN_vkAllocateMemory)getDeviceProcAddr(*pDevice, "vkAllocateMemory");
   device.freeMemory = (PFN_vkFreeMemory)getDeviceProcAddr(*pDevice, "vkFreeMemory");
   device.createImage = (PFN_vkCreateImage)getDeviceProcAddr(*pDevice, "vkCreateImage");
   device.destroyImage = (PFN_vkDestroyImage)getDeviceProcAddr(*pDevice, "vkDestroyImage");
   device.getImageMemoryRequirements = (PFN_vkGetImageMemoryRequirements)getDeviceProcAddr(*pDevice, "vkGetImageMemoryRequirements");
   device.getImageMemoryRequirements2 = (PFN_vkGetImageMemoryRequirements2)getDeviceProcAddr(*pDevice, "vkGetImageMemoryRequirements2");
   if (!device.getImageMemoryRequirements2)
      device.getImageMemoryRequirements2 = (PFN_vkGetImageMemoryRequirements2)getDeviceProcAddr(*pDevice, "vkGetImageMemoryRequirements2KHR");

   device.bindImageMemory = (PFN_vkBindImageMemory)getDeviceProcAddr(*pDevice, "vkBindImageMemory");
   device.bindImageMemory2 = (PFN_vkBindImageMemory2)getDeviceProcAddr(*pDevice, "vkBindImageMemory2");
   if (!device.bindImageMemory2)
      device.bindImageMemory2 = (PFN_vkBindImageMemory2)getDeviceProcAddr(*pDevice, "vkBindImageMemory2KHR");

   {
      std::unique_lock<std::mutex> l(g_mutex);
      g_device[*pDevice] = device;
   }
   return VK_SUCCESS;
}

static void
layer_DestroyDevice(VkDevice device,
                    const VkAllocationCallbacks* pAllocator)
{
   std::unique_lock<std::mutex> l(g_mutex);
   auto it = g_device.find(device);
   assert(it != g_device.end());

   it->second.destroyDevice(device, pAllocator);
   g_device.erase(it);
}

static VkResult
layer_CreateInstance(const VkInstanceCreateInfo* pCreateInfo,
                     const VkAllocationCallbacks* pAllocator,
                     VkInstance* pInstance)
{
   VkLayerInstanceCreateInfo *layer_create_info = (VkLayerInstanceCreateInfo *)pCreateInfo->pNext;

   while(layer_create_info &&
         (layer_create_info->sType != VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO ||
          layer_create_info->function != VK_LAYER_LINK_INFO)) {
      layer_create_info = (VkLayerInstanceCreateInfo *)layer_create_info->pNext;
   }

   if(layer_create_info == NULL)
      return VK_ERROR_INITIALIZATION_FAILED;

   PFN_vkGetInstanceProcAddr getInstanceProcAddr = layer_create_info->u.pLayerInfo->pfnNextGetInstanceProcAddr;
   layer_create_info->u.pLayerInfo = layer_create_info->u.pLayerInfo->pNext;

   PFN_vkCreateInstance createInstance = (PFN_vkCreateInstance)getInstanceProcAddr(VK_NULL_HANDLE, "vkCreateInstance");

   VkResult result = createInstance(pCreateInfo, pAllocator, pInstance);
   if (result != VK_SUCCESS)
      return result;

   Instance instance = {};
   instance.getInstanceProcAddr = getInstanceProcAddr;
   instance.destroyInstance = (PFN_vkDestroyInstance)getInstanceProcAddr(*pInstance, "vkDestroyInstance");
   instance.createDevice = (PFN_vkCreateDevice)getInstanceProcAddr(*pInstance, "vkCreateDevice");
   instance.destroyDevice = (PFN_vkDestroyDevice)getInstanceProcAddr(*pInstance, "vkDestroyDevice");

   {
      std::unique_lock<std::mutex> l(g_mutex);
      g_instance[*pInstance] = instance;
   }
   return VK_SUCCESS;
}

static void
layer_DestroyInstance(VkInstance instance,
                      const VkAllocationCallbacks* pAllocator)
{
   std::unique_lock<std::mutex> l(g_mutex);
   auto it = g_instance.find(instance);
   assert(it != g_instance.end());

   it->second.destroyInstance(instance, pAllocator);
   g_instance.erase(it);
}

#define GETPROCADDR(func) if(!strcmp(funcName, "vk" #func)) return (PFN_vkVoidFunction)&layer_##func;
#define GETPROCADDRKHR(func) if(!strcmp(funcName, "vk" #func "KHR")) return (PFN_vkVoidFunction)&layer_##func;

extern "C" VK_LAYER_EXPORT VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL layer_GetDeviceProcAddr(VkDevice dev,
                                                                                 const char *funcName)
{
   GETPROCADDR(GetDeviceProcAddr);
   GETPROCADDR(CreateDevice);
   GETPROCADDR(DestroyDevice);

   GETPROCADDR(AllocateMemory);
   GETPROCADDR(FreeMemory);
   GETPROCADDR(CreateImage);
   GETPROCADDR(DestroyImage);
   GETPROCADDR(GetImageMemoryRequirements);
   GETPROCADDR(GetImageMemoryRequirements2);
   GETPROCADDRKHR(GetImageMemoryRequirements2);
   GETPROCADDR(BindImageMemory);
   GETPROCADDR(BindImageMemory2);
   GETPROCADDRKHR(BindImageMemory2);

   if (dev == NULL) return NULL;

   {
      std::unique_lock<std::mutex> l(g_mutex);
      auto it = g_device.find(dev);
      if (it == g_device.end())
         return NULL;

      if (!it->second.getDeviceProcAddr)
         return NULL;

      return it->second.getDeviceProcAddr(dev, funcName);
   }
}

extern "C" VK_LAYER_EXPORT VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL layer_GetInstanceProcAddr(VkInstance instance,
                                                                                   const char *funcName)
{
   GETPROCADDR(CreateInstance);
   GETPROCADDR(DestroyInstance);

   GETPROCADDR(GetDeviceProcAddr);
   GETPROCADDR(CreateDevice);
   GETPROCADDR(DestroyDevice);

   GETPROCADDR(AllocateMemory);
   GETPROCADDR(FreeMemory);
   GETPROCADDR(CreateImage);
   GETPROCADDR(DestroyImage);
   GETPROCADDR(GetImageMemoryRequirements);
   GETPROCADDR(GetImageMemoryRequirements2);
   GETPROCADDRKHR(GetImageMemoryRequirements2);
   GETPROCADDR(BindImageMemory);
   GETPROCADDR(BindImageMemory2);
   GETPROCADDRKHR(BindImageMemory2);

   if (instance == NULL) return NULL;

   {
      std::unique_lock<std::mutex> l(g_mutex);
      auto it = g_instance.find(instance);
      if (it == g_instance.end())
         return NULL;

      if (!it->second.getInstanceProcAddr)
         return NULL;

      return it->second.getInstanceProcAddr(instance, funcName);
   }
}
